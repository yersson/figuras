package com.mycompany.mavenproject1;

/**
 *
 * @author sigmotoa
 */
public class TestFiguras {
    public static void main(String[] args) {
        
        Circulo c=new Circulo(3);
        Triangulo t=new Triangulo(3, 3);
        Rectangulo r=new Rectangulo(3, 3);
        
        System.out.println("\033[35m [Circulo]\n"+c);
        System.out.println("\033[35m [Triangulo]\n"+t);
        System.out.println("\033[35m [Rectangulo]\n"+r);
    }
    
}
