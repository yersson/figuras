
package com.mycompany.mavenproject1;

/**
 *
 * @author sigmotoa
 */
public abstract class FiguraGeometrica {
    
    public abstract double area();
    public abstract double perimetro();
    public abstract double volumen();
    
    @Override
    public String toString (){
        
        return "\n AREA: "+area()+"\n"+
                "\n"+
               "PERIMETRO: "+perimetro()+"\n"+
                "\n"+
               "VOLUMEN: "+volumen()+"\n"
                ;
                
    } 
    
        
    
    
}
