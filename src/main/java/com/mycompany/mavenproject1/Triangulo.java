package com.mycompany.mavenproject1;

/**
 *
 * @author sigmotoa
 */
public class Triangulo extends FiguraGeometrica{

private double base;
private double altura;

    public Triangulo(double base, double altura) {
        this.base = base;
        this.altura = altura;
    }

    @Override
    public double area()
    {
        return (base*altura)/2;
    }
    
    //perimetro
    @Override
    public double perimetro(){
        double hipotenusa= Math.sqrt(Math.pow(base,2)+Math.pow(altura,2));
        return hipotenusa+altura+base;
    }
    
    //volumen
    @Override
    public double volumen(){
        return (1/3)*base*altura;
    }

    public double getBase() {
        return base;
    }

    public void setBase(double base) {
        this.base = base;
    }

    public double getAltura() {
        return altura;
    }

    public void setAltura(double altura) {
        this.altura = altura;
    }
    
    
    
}
